# tks-network-adapter

## Description

The Movie Database (TMDb) network feature, interface adapters layer, CMP common project, providing Android, iOS and Desktop targets.

This project defines TMDb network interfaces and data classes in the adapter Clean Architecture layer. It exists to specify these
artifacts which are used by other TMDb interfaces and by outer layers to provide data acquired from the TMDb database.

## License

GPL, Version 3.0.  See the peer document LICENSE for details.

## Contributions

See the [contributing guide](https://gitlab.com/pajato/argus/argus-doc/-/blob/main/CONTRIBUTING.md) in the
[Argus Documentation Project](https://gitlab.com/pajato/argus/argus-doc/-/blob/main/README.md).

## Project status

Converted to Kotlin Multiplatform (KMP) with versions 0.10.*

## Documentation

For general documentation on the TMDb API, see
[The Movie Database API](https://developers.themoviedb.org/3/getting-started/introduction).

As documentation entered into code files grows stale seconds after it is written, no such documentation is created.
Instead, documentation is created by you on demand using the Dokka Gradle task: 'dokkaGfm'. After successful task
completion, see the detailed documentation [here](build/dokka/gfm/index.md)

## Usage

To use the project, follow these steps:

1. Add the project as a dependency in your build file.
2. Import the necessary classes and interfaces from the project.
3. Use the provided APIs to interact with the shelf feature.

## Test Cases

The table below identifies the adapter layer unit tests. A test file name is always of the form `NamePrefixUnitTest.kt`.
The test file content is one or more test cases (functions)

| Filename Prefix   | Test Case Name                                                     |
|-------------------|--------------------------------------------------------------------|
| NetworkRepo       | When accessing a network, verify behavior                         |
|                   | When accessing a non-cached network, verify behavior               |
|                   | When simulating a network fetch without injection, verify behavior |

### Overview

### Notes

The single responsibility for this project is to provide the TMDb network artifact definitions used by this
and outer architectural layers. The adapter layer implements these core network interfaces.
